package com.id11427390.exercise3;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class Exercise3 extends Activity {

    EditText name;
    EditText number;
    String subjectName;
    String subjectNumber;
    String newName = subjectName;
    String newNumber = subjectNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise3);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            subjectName = savedInstanceState.getString(newName);
            subjectNumber = savedInstanceState.getString(newNumber);
        } else {
            // Probably initialize members with default values for a new instance
        }

        name = (EditText) findViewById(R.id.subjectName);
        number = (EditText)findViewById(R.id.subjectNumber);
        subjectName = name.getText().toString();
        subjectNumber = number.getText().toString();
        ImageView mainImage = (ImageView) findViewById(R.id.imageViewMain);
        mainImage.setImageResource(R.drawable.ic_launcher);
        RadioButton coreRadio = (RadioButton) findViewById(R.id.coreRadio);
        Button rotate = (Button) findViewById(R.id.rotateBtn);
        coreRadio.setChecked(true);

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View arg0, boolean focusSelect) {
                if (focusSelect) {
                    Context context = getApplicationContext();
                    CharSequence text = "Subject name HAS focus!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast.makeText(context, text, duration).show();
                }
                else
                {
                    Context context = getApplicationContext();
                    CharSequence text = "Subject name LOST focus!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast.makeText(context, text, duration).show();
                }
            }
        });

        rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString(newName, subjectName);
        savedInstanceState.putString(newNumber, subjectNumber);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore state members from saved instance
        subjectName = savedInstanceState.getString(newName);
        subjectNumber = savedInstanceState.getString(newNumber);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
};