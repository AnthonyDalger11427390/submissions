package com.id11427390.exercise6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.Subject;

/**
 * Created by Anthony on 2/09/2014.
 */

public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "subjects";
    private static final int DB_VERSION = 1;

    public MyDatabaseHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DB_NAME, factory, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Subjects (id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "subjectname TEXT,subjectnumber TEXT, iscore INTEGER, startdate INTEGER)");
        db.execSQL("INSERT INTO Subjects (id, subjectname,subjectnumber,iscore,startdate) VALUES" +
                "('0', 'SubjectOne', '00001111', '20', '12/12/2014')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    /*
     *  To update database when new versions are released
     *
     *  switch (oldVersion) {
     *      case 1:
     *      case 2:
     *  }
     */
    }

		// Ryan: This does not override any method from the superclass or interface
    //@Override
    public void addSubject(Subject subject){
        //Ryan: spelling mistake
        //SQLiteDatabase db = getWritableDatatbase();
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

				// Ryan: You are making reference to a Subject class which you never defined.
        //calls respective class to add selected field
        values.put("subjectname", subject.getName());
        values.put("subjectnumber", subject.getNumber());
        values.put("subjectscore", subject.getScore());
        values.put("subjectdate", subject.getDate());
        values.put("subjectid", subject.getId());

        db.insert(DB_NAME, null, null);
    }

    public Subject getSubject(int id){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_NAME, new String[] {"subjectname", "subjectnumber", "subjectscore",
                "subjectdate", "subjectid"}, null, new String[] { String.valueOf(id) }, null, null, null);

        return getSubject(id);
    }

    public List<SubjectActivity> getAllSubjects(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(DB_NAME, new String[] {"name", "number"}, null, null, null, null, null);
        cursor.moveToFirst();
        List<SubjectActivity> subjects = new ArrayList<SubjectActivity>();
        do{
            subjects.add(new SubjectActivity(cursor.getString(1), cursor.getString(2),
                    cursor.getInt(3), cursor.getInt(4), cursor.getInt(5)));
        }
        while(cursor.moveToNext());
        //search database for fields, return all subjects
        return subjects;
        }
    }
