package com.id11427390.exercise6;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class SubjectActivity extends Activity {

	// Ryan: These fiedls should be moved into a separate "Subject" class representing the model.
    private String subjectName;
    private String subjectNumber;
    private int iScore;
    private int startDate;
    private int id;

    EditText addSubjectName;
    EditText addSubjectNumber;
    DatePicker addStartDate;
    RadioButton coreCheck;
    RadioButton electiveCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        addSubjectName = (EditText) findViewById(R.id.subjectName);
        addSubjectNumber = (EditText) findViewById(R.id.subjectNumber);
        addStartDate = (DatePicker) findViewById(R.id.startDate);
        coreCheck = (RadioButton) findViewById(R.id.coreRadio);
        electiveCheck = (RadioButton) findViewById(R.id.electiveRadio);

        Button addButton = (Button)findViewById(R.id.addBtn);
        Button listButton = (Button) findViewById(R.id.listBtn);

        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context context = getApplicationContext();
                CharSequence text = "Subject Successfully Added";
                int duration = Toast.LENGTH_SHORT;

                Toast.makeText(context, text, duration).show();
            }
        });

        listButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent newIntent = new Intent(SubjectActivity.this, MySubjectListActivity.class);
                startActivity(newIntent);
            }
        });
    }

    public SubjectActivity(String addSubjectName, String addSubjectNumber, int iScore, int addStartDate, int id){
        //fields should be set here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise6, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(SubjectActivity.this, MyPreferenceActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
