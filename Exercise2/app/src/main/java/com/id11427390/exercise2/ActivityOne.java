package com.id11427390.exercise2;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ActivityOne extends Activity {
    String radioSelection;
    //radioSelective determines the string to be passed through to the next activity

    private Context actOne = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_activity_one);
            Button showA1 = (Button) findViewById(R.id.showA1Btn);
            showA1.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v) {
                    EditText name = (EditText) findViewById(R.id.subjectName);
                    EditText number = (EditText)findViewById(R.id.subjectNumber);
                    RadioButton coreCheck = (RadioButton) findViewById(R.id.coreRadio);
                    RadioButton electiveCheck = (RadioButton) findViewById(R.id.electiveRadio);
                    String subjectNumber = number.getText().toString();
                    String subjectName = name.getText().toString();

                    //Variable to determine the string to be passed through to the next activity
                    if (coreCheck.isChecked()) {
                        radioSelection = "Core";
                    }
                    else if (electiveCheck.isChecked()) {
                        radioSelection = "Elective";
                    }
                    else {
                        //Toast made for when no radio button is selected
                        Context context = getApplicationContext();
                        CharSequence text = "Please check a Radio Button!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, text, duration).show();
                    }

										// Ryan: This if condition is redundant since you already know which button was clicked.
                    if (v.getId() == R.id.showA1Btn) {
                        //Check to see whether or not a subjectName or subjectNumber has been inputted
                        if (subjectName.equals("")) {
                            Context context = getApplicationContext();
                            CharSequence text = "Subject Name is empty!";
                            int duration = Toast.LENGTH_SHORT;

                            Toast.makeText(context, text, duration).show();
                        }
                        else if (subjectNumber.equals("")) {
                            Context context = getApplicationContext();
                            CharSequence text = "Subject Number is empty!";
                            int duration = Toast.LENGTH_SHORT;

                            Toast.makeText(context, text, duration).show();
                        }
                    }

                    // If there is a valid name, number and radio selection, a new intent is started for ActivityTwo.class
                    if ((!subjectName.equals("")) && (!subjectNumber.equals("")) && ((coreCheck.isChecked()) || (electiveCheck.isChecked()))){
                        //create the intent for the activity
                        Intent newIntent = new Intent(actOne, ActivityTwo.class);
                        //pass through values as strings to activityTwo
                        newIntent.putExtra("subjectName", name.getText().toString());
                        newIntent.putExtra("subjectNumber", number.getText().toString());
                        newIntent.putExtra("radioChoice", radioSelection);
                        //start ActivityTwo
                        startActivity(newIntent);
                    }
                }
            });
            Button showA2 = (Button) findViewById(R.id.showA2Btn);
            //does nothing yet
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
