package com.id11427390.exercise2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class ActivityTwo extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_two);

        TextView nameText = (TextView) findViewById(R.id.subjectNameTwo);
        TextView numberText = (TextView) findViewById(R.id.subjectNumberTwo);
        TextView radioText = (TextView) findViewById(R.id.radioChoice);

        //get intent, and values passed through from last class
        Intent intentTwo = getIntent();
        final String intentName = intentTwo.getStringExtra("subjectName");
        final String intentNumber = intentTwo.getStringExtra("subjectNumber");
        final String intentRadio = intentTwo.getStringExtra("radioChoice");

        //set new text fields with new values passed through
        nameText.setText(intentName);
        numberText.setText(intentNumber);
        radioText.setText(intentRadio);

        Button popupBtn = (Button) findViewById(R.id.popupButton);
            popupBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Alert dialog builder for popup button
                    AlertDialog.Builder popupBuilder = new AlertDialog.Builder(ActivityTwo.this);
                        //Information to be displayed on popup
                        popupBuilder.setTitle("Subject"); // Ryan: Don't hardcode this string.
                        popupBuilder.setMessage("Subject Name: " + intentName + "\n" + "Subject Number: " + intentNumber + "\n" + "Type: " + intentRadio);
                        popupBuilder.setPositiveButton(R.string.popupString, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                    // type what okay button does here!
                            }
                        });
                        //popupBuilder.setIcon(android.R.drawable.ic_dialog_alert);
                        //For future reference, this is to set an icon for the popup
                    AlertDialog alertPopup = popupBuilder.create();
                    alertPopup.show();
                }
            });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
