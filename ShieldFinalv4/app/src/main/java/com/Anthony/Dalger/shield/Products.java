package com.Anthony.Dalger.shield;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * Products Class
 * Displays product categories for selection,
 * leading to product lists.
 */

public class Products extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        /** Set Logo transparency */
        ImageView mainScreenLogo = (ImageView) findViewById(R.id.logoIMG);
        mainScreenLogo.setAlpha(50);

        /** Set Logo in background depending on screen orientation,
         * as well as width and height.
         */
        int orientation = getResources().getConfiguration().orientation;
        if (Configuration.ORIENTATION_LANDSCAPE == orientation) {
            mainScreenLogo.setImageResource(R.drawable.body_3);
            mainScreenLogo.getLayoutParams().height = 1000;
            mainScreenLogo.getLayoutParams().width = 1600;
        } else if (Configuration.ORIENTATION_PORTRAIT == orientation) {
            mainScreenLogo.setImageResource(R.drawable.body_3_3);
            mainScreenLogo.getLayoutParams().height = 1000;
            mainScreenLogo.getLayoutParams().width = 1000;
        }

        /** Retrieve sources from layout */
        Button accessoriesBtn = (Button)findViewById(R.id.accessoriesBtn);
        Button beltsBtn = (Button)findViewById(R.id.beltsBtn);
        Button glovesBtn = (Button)findViewById(R.id.glovesBtn);
        Button wrapsBtn = (Button)findViewById(R.id.wrapsStrapsBtn);
        Button packsBtn = (Button)findViewById(R.id.gymPacksBtn);

        // On Button Click, Create an intent to view a list of accessories
        accessoriesBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentAccessories = new Intent(Products.this, MyListActivity.class);
                intentAccessories.putExtra((getString(R.string.product_ID)),
                        (getString(R.string.accessories)));
                startActivity(intentAccessories);
            }
        });

        // On Button Click, Create an intent to view a list of belts
        beltsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentBelts = new Intent(Products.this, MyListActivity.class);
                intentBelts.putExtra((getString(R.string.product_ID)),
                        (getString(R.string.belts)));
                startActivity(intentBelts);
            }
        });

        // On Button Click, Create an intent to view a list of gloves
        glovesBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentGloves = new Intent(Products.this, MyListActivity.class);
                intentGloves.putExtra((getString(R.string.product_ID)),
                        (getString(R.string.gloves)));
                startActivity(intentGloves);
            }
        });

        // On Button Click, Create an intent to view a list of wraps and straps
        wrapsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentWraps = new Intent(Products.this, MyListActivity.class);
                intentWraps.putExtra((getString(R.string.product_ID)),
                        (getString(R.string.wraps)));
                startActivity(intentWraps);
            }
        });

        // On Button Click, Create an intent to view a list of gym packs
        packsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentPacks = new Intent(Products.this, MyListActivity.class);
                intentPacks.putExtra((getString(R.string.product_ID)),
                        (getString(R.string.packs)));
                startActivity(intentPacks);
            }
        });
    }

    /** Create Options Menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shield, menu);
        return true;
    }

    /** Add Option Menu Items */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /** Opens About class to view About Us */
            case R.id.menu_about_us:
                Intent aboutIntent = new Intent(Products.this, About.class);
                startActivity(aboutIntent);
                break;

            /** Directs the user to Shield Fitness Home page */
            case R.id.menu_website:
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.shield_website)));
                startActivity(i);
                break;

            /** Directs the user to Settings and Preferences */
            case R.id.menu_settings:
                Intent homeIntent = new Intent(Products.this, MyPreferenceActivity.class);
                startActivity(homeIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}