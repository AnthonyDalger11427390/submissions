package com.Anthony.Dalger.shield;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * WorkoutDescription Class
 * Displays specific workouts selected in the
 * WorkoutListActivity screen. This screen shows the
 * workout, an appropriate video, appropriate workout
 * description, and a diagram showing which body part
 * is trained.
 */

public class WorkoutDescription extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {

    // Registered for YouTube Developer Key
    static private final String DEVELOPER_KEY = "AIzaSyBBYufnW5o7rxdmqSNf6Cpf-0e1PYq_oP8";
    static private String sitUpVIDEO = "jDwoBqPH0jk";
    static private String ultimateVIDEO = "fani-Ck4ipk";
    static private String alterDeltoidVIDEO = "2ZS1Ae5n90M";
    static private String barbellBicepVIDEO = "kwG2ipFRgfo";
    static private String declinedDumbVIDEO = "mlXPNdw2Sns";
    static private String reversePullVIDEO = "apzFTbsm7HU";
    static private String reverseLungeVIDEO = "NfMIqYNNLl8";

    static private String VIDEO;
    static private String NEW_VIDEO;
    TextView workoutText;
    TextView workoutDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_description);

        /** Retrieve sources from layout */
        workoutText = (TextView)findViewById(R.id.textView);
        workoutDescription = (TextView)findViewById(R.id.descriptionText);
        ImageView mainBody = (ImageView) findViewById(R.id.bodyPart);

        /** Receive value passed from Workouts screen */
        Bundle bundle = getIntent().getExtras();
        String importID = bundle.getString((getString(R.string.workout_ID)));

        /** Set imported value to string */
        workoutText.setText(String.valueOf(importID));

        /** Set YouTube Video ID, description and image depending
         * on received value.
         */
        if (importID.equals((getString(R.string.sit_ups)))) {
            VIDEO = sitUpVIDEO;
            workoutDescription.setText((getString(R.string.sit_up_description)));
            mainBody.setImageResource(R.drawable.abs);
        } else if (importID.equals((getString(R.string.ultimate_beginners)))) {
            VIDEO = ultimateVIDEO;
            workoutDescription.setText((getString(R.string.ultimate_beginners_description)));
            mainBody.setImageResource(R.drawable.full);
        } else if (importID.equals((getString(R.string.alter_deltoid_raise)))) {
            VIDEO = alterDeltoidVIDEO;
            workoutDescription.setText((getString(R.string.alter_deltoid_raise_description)));
            mainBody.setImageResource(R.drawable.shoulders);
        } else if (importID.equals((getString(R.string.barbell_bicep)))) {
            VIDEO = barbellBicepVIDEO;
            workoutDescription.setText((getString(R.string.barbell_bicep__description)));
            mainBody.setImageResource(R.drawable.arms);
        } else if (importID.equals((getString(R.string.declined_dumb_press)))) {
            VIDEO = declinedDumbVIDEO;
            workoutDescription.setText((getString(R.string.declined_dumb_press_description)));
            mainBody.setImageResource(R.drawable.chest);
        } else if (importID.equals((getString(R.string.reverse_pull_down)))) {
            VIDEO = reversePullVIDEO;
            workoutDescription.setText((getString(R.string.reverse_pull_down_description)));
            mainBody.setImageResource(R.drawable.back);
        } else if (importID.equals((getString(R.string.reverse_lunge)))) {
            VIDEO = reverseLungeVIDEO;
            workoutDescription.setText((getString(R.string.reverse_lunge_description)));
            mainBody.setImageResource(R.drawable.legs);
        }

        /** Set body image dimensions */
        mainBody.getLayoutParams().height = 675;
        mainBody.getLayoutParams().width = 338;

        /** Set final video for viewing */
        NEW_VIDEO = VIDEO;

        /** Initialise YouTube Player and Developer Key */
        YouTubePlayerView youTubeView = (YouTubePlayerView)
                findViewById(R.id.youtube_view);
        youTubeView.initialize(DEVELOPER_KEY, this);
    }

    /** Set Video Failure Message in case Video fails to load */
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult error) {
        Toast.makeText(this, (getString(R.string.youtube_error))
                        + error.toString(),
                Toast.LENGTH_LONG).show();
    }

    /** Load final video for viewing */
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        player.loadVideo(NEW_VIDEO);
    }
}