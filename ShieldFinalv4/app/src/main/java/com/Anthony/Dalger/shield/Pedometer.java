package com.Anthony.Dalger.shield;

import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * Pedometer Class
 * Shows the pedometer, which is used to count the
 * users steps, and calculate calories lost.
 */

public class Pedometer extends Activity {

    /** Variables for Accelerometer */
    private TextView textViewX;
    private TextView textViewY;
    private TextView textViewZ;

    /** Variable for Sensitivity */
    private TextView textSensitive;

    /** Variable for Step Display */
    private TextView textViewSteps;

    /** Buttons */
    private Button buttonReset;
    private Button buttonCalories;

    /** Variables for Sensor */
    private SensorManager sensorManager;
    private float acceleration;
    private SeekBar seekBar;

    /** Variables for Step Calculations */
    private float previousY;
    private float currentY;
    private float previousX;
    private float currentX;
    private int numSteps;
    private int AVG_STEPS;
    private int threshold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedometer);

        /** Retrieve sources from layout */
        textViewX = (TextView)findViewById(R.id.textViewX);
        textViewY = (TextView)findViewById(R.id.textViewY);
        textViewZ = (TextView)findViewById(R.id.textViewZ);
        textViewSteps = (TextView)findViewById(R.id.textSteps);
        textSensitive = (TextView)findViewById(R.id.textSensitive);
        buttonReset = (Button)findViewById(R.id.buttonReset);
        buttonCalories = (Button)findViewById(R.id.buttonCalories);
        seekBar = (SeekBar)findViewById(R.id.seekBar);

        /** Initialise and set SeekBar and Threshold */
        seekBar.setProgress(10);
        seekBar.setOnSeekBarChangeListener(seekBarListener);
        threshold = 10;
        textSensitive.setText(String.valueOf(threshold));

        /** Set variable values */
        previousY = 0;
        currentY = 0;
        previousX = 0;
        currentX = 0;
        numSteps = 0;
        AVG_STEPS = 6201;
        acceleration = 0.00f;

        /** Call enableAccelerometerListening () */
        enableAccelerometerListening ();

        /** Get Preferences to do calculations */
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String weight = preferences.getString("weight", "");
        final int calcWeight = Integer.parseInt(weight);
        String height = preferences.getString("height", "");
        final int calcHeight = Integer.parseInt(height);
        String age = preferences.getString("age", "");
        final int calcAge = Integer.parseInt(age);
        final double finalWeight = (calcWeight * 2.20462);
        final double finalHeight = (calcHeight * 0.393700787);

        /** Calculate calories burnt when calories butto pressed */
        buttonCalories.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                double calories = ((66 + (6.23 * finalWeight) + (127 * finalHeight) + (6.8 * calcAge)));
                double steps = (calories/AVG_STEPS);
                final double finalCalories = (steps*numSteps)/100;
                final double roundCalories = Math.round(finalCalories*100.0)/100.0;
                final String caloriesFinal = String.valueOf(roundCalories);
                Toast.makeText(getApplicationContext(), "You have burnt " + caloriesFinal + " Calories!",
                    Toast.LENGTH_LONG).show();
            }
        });
    }

    /** Access device Sensors */
    public void enableAccelerometerListening () {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    /** Adjust steps based on sensor */
    private SensorEventListener sensorEventListener =
            new SensorEventListener() {

                @Override
                public void onSensorChanged (SensorEvent event) {
                    float x = event.values[0];
                    float y = event.values[1];
                    float z = event.values[2];

                    currentY = y;
                    currentX = x;

                    // Check to see if step has been taken
                    if (Math.abs (currentY - previousY) > threshold) {
                        numSteps ++;
                        textViewSteps.setText(String.valueOf(numSteps));
                    } else if (Math.abs (currentX - previousX) > threshold) {
                        numSteps ++;
                        textViewSteps.setText(String.valueOf(numSteps));
                    }

                    textViewX.setText(String.valueOf(x));
                    textViewY.setText(String.valueOf(y));
                    textViewZ.setText(String.valueOf(z));

                    previousY = y;
                    previousX = x;
                }

                public void onAccuracyChanged (Sensor sensor, int accuracy) {

                }
            };

    /** Reset steps to zero */
    public void resetSteps (View v) {
        numSteps = 0;
        textViewSteps.setText(String.valueOf(numSteps));
    }

    /** Adjust threshold via SeekBar */
    private OnSeekBarChangeListener seekBarListener =
            new OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    threshold = seekBar.getProgress();
                    textSensitive.setText(String.valueOf(threshold));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };
}
