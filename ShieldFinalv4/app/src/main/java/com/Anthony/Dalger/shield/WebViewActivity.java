package com.Anthony.Dalger.shield;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * WebView Class
 * Displays selected webpage inside a webview.
 */

public class WebViewActivity extends Activity {

    private String webID;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        /** Receive value passed MyListActivity */
        Bundle bundle = getIntent().getExtras();
        String importID = bundle.getString((getString(R.string.web_ID)));

        /** Convert received value to string */
        webID = importID.toString();

        /** Retrieve sources from layout */
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);

        /** Load webID as URL */
        webView.loadUrl(webID);
    }
}
