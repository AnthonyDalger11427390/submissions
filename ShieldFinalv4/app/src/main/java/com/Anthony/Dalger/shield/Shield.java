package com.Anthony.Dalger.shield;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * Shield Class
 * This is the opening screen, shown upon app
 * start. This screen allows the user to login
 * using facebook, twitter, or as a guest.
 */

public class Shield extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shield);

        /** Retrieve sources from layout */
        Button guestBtn = (Button)findViewById(R.id.guestLogin);
        Button facebookBtn = (Button) findViewById(R.id.facebookBtn);
        Button twitterBtn = (Button) findViewById(R.id.twitterBtn);
        ImageView mainScreenLogo = (ImageView) findViewById(R.id.logoIMG);

        /** Set Shield Fitness Logo in background, as well as width,
         * height and transparency.
         */
        mainScreenLogo.setAlpha(50);
        mainScreenLogo.setImageResource(R.drawable.shield_fitness);
        mainScreenLogo.getLayoutParams().height = 1000;
        mainScreenLogo.getLayoutParams().width = 1000;

        /** Facebook Login Button - Was unable to get Facebook
         * interaction working so a toast message is put in place
         */
        facebookBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), getString(R.string.facebook_login),
                        Toast.LENGTH_SHORT).show();
            }
        });

        /** Twitter Login Button - Was unable to get Twitter
         * interaction working so a toast message is put in place
         */
        twitterBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), getString(R.string.twitter_login),
                        Toast.LENGTH_SHORT).show();
            }
        });

        /** Guest Login Button - Logs the user in to the main screen */
        guestBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), getString(R.string.guest_login),
                        Toast.LENGTH_SHORT).show();

                Intent productsIntent = new Intent(Shield.this, Main.class);
                startActivity(productsIntent);
            }
        });
    }
}