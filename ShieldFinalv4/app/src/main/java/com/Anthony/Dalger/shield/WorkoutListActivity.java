package com.Anthony.Dalger.shield;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * WorkoutListActivity Class
 * Displays results of Workouts screen. This screen then
 * leads the user to the selected workout in the next
 * screen.
 */

public class WorkoutListActivity extends Shield
{
    private String workoutID;
    ArrayList<String> resultsList;

    public void onCreate(Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_workout_list);

        // Receive value passed from Workouts screen
        Bundle bundle = getIntent().getExtras();
        String importID = bundle.getString((getString(R.string.workout_ID)));

        // Convert received value to String
        workoutID = importID.toString();

        /** Retrieve sources from layout */
        ListView newList = (ListView)findViewById(R.id.listViewResults);

        // Create Array for results, get list
        resultsList = new ArrayList<String>();
        getList();

        // Create an Adapter; pass resultsList Array into the Adapter
        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, resultsList);
        newList.setAdapter(arrayAdapter);

        /* On List Item Click, Check to see which position the item is in the Array.
        Store the value of the List Item as selectedWorkout, then match the workout with string
        Depending on the string value, a different workout description will be shown
        */
        newList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View v,int position, long arg3)
            {
                String selectedWorkout = resultsList.get(position);
                Intent i = new Intent(WorkoutListActivity.this, WorkoutDescription.class);
                if (selectedWorkout == (getString(R.string.ultimate_beginners))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.ultimate_beginners)));
                }
                else if (selectedWorkout == (getString(R.string.alter_deltoid_raise))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.alter_deltoid_raise)));
                }
                else if (selectedWorkout == (getString(R.string.barbell_bicep))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.barbell_bicep)));
                }
                else if (selectedWorkout == (getString(R.string.declined_dumb_press))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.declined_dumb_press)));
                }
                else if (selectedWorkout == (getString(R.string.reverse_pull_down))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.reverse_pull_down)));
                }
                else if (selectedWorkout == (getString(R.string.sit_ups))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.sit_ups)));
                }
                else if (selectedWorkout == (getString(R.string.reverse_lunge))) {
                    i.putExtra((getString(R.string.workout_ID)),
                            (getString(R.string.reverse_lunge)));
                }
                startActivity(i);
            }
        });
    }

    void getList()
    {
        // List Items added depending on what button was pressed on previous screen
        if (workoutID.equals((getString(R.string.full)))) {
            resultsList.add((getString(R.string.ultimate_beginners)));
        }
        else if (workoutID.equals((getString(R.string.shoulders)))) {
            resultsList.add((getString(R.string.alter_deltoid_raise)));
        }
        else if (workoutID.equals((getString(R.string.arms)))) {
            resultsList.add((getString(R.string.barbell_bicep)));
        }
        else if (workoutID.equals((getString(R.string.chest)))) {
            resultsList.add((getString(R.string.declined_dumb_press)));
        }
        else if (workoutID.equals((getString(R.string.back)))) {
            resultsList.add((getString(R.string.reverse_pull_down)));
        }
        else if (workoutID.equals((getString(R.string.abs)))) {
            resultsList.add((getString(R.string.sit_ups)));
        }
        else if (workoutID.equals((getString(R.string.legs)))) {
            resultsList.add((getString(R.string.reverse_lunge)));
        }
    }
}