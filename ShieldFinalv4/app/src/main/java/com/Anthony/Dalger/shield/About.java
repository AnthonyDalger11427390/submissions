package com.Anthony.Dalger.shield;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * About Class
 * Displays information about Shield Fitness, as well as
 * provide details on how to contact the company. The
 * company address and map is also provided.
 */

public class About extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        /** Retrieve sources from layout */
        Button phoneBtn = (Button)findViewById(R.id.phoneBtn);
        Button emailBtn = (Button)findViewById(R.id.emailBtn);
        Button mapBtn = (Button)findViewById(R.id.mapBtn);
        ImageView mainScreenLogo = (ImageView) findViewById(R.id.logoIMG);

        /** Set Shield Fitness Logo in background, as well as width,
         * height and transparency.
         */
        mainScreenLogo.setAlpha(50);
        mainScreenLogo.setImageResource(R.drawable.shield_logo);
        mainScreenLogo.getLayoutParams().height = 1000;
        mainScreenLogo.getLayoutParams().width = 1000;

        /** Open Call Menu and prepare number to call */
        phoneBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentCall = new Intent(android.content.Intent.ACTION_DIAL,
                        Uri.parse("tel:0422704344"));
                startActivity(intentCall);
            }
        });

        /** View Address via latitude and longitude in and
         * external application.
         */
        mapBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentCall = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("geo:-33.881720, 150.773099"));
                startActivity(intentCall);
            }
        });

        /** Open an external email application to send email
         * to Shield. Mail To Email is already set, as is
         * the Subject field.
         */
        emailBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentEmail = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","info@shieldfitness.com.au", null));
                intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Shield App Enquiry");
                startActivity(Intent.createChooser(intentEmail, "Send email"));
            }
        });
    }
}