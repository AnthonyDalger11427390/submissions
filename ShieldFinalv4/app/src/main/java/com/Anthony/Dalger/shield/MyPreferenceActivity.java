package com.Anthony.Dalger.shield;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * MyPreferenceActivity Class
 * The preference and settings screen of the app. The
 * user can input their gender, age, weight and height,
 * and their input will affect certain features in the
 * app.
 */

public class MyPreferenceActivity extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);

        /** Create ListPreference for Gender */
        ListPreference genderList = (ListPreference) findPreference("gender");
        genderList.setSummary(genderList.getEntry());
        genderList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener(){
            @Override
            public boolean onPreferenceChange(Preference preference, Object object){
                preference.setSummary(object.toString());
                return true;
            }
        });

        /** Create EditTextPreference for Weight */
        EditTextPreference weight = (EditTextPreference) findPreference("weight");
        weight.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener(){
            @Override
            public boolean onPreferenceChange(Preference preference, Object object){
                preference.setSummary(object.toString());
                return true;
            }
        });

        /** Create EditTextPreference for Height */
        final EditTextPreference height = (EditTextPreference) findPreference("height");
        height.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener(){
            @Override
            public boolean onPreferenceChange(Preference preference, Object object){
                preference.setSummary(height.getText());
                return true;
            }
        });

        /** Create EditTextPreference for Age */
        EditTextPreference age = (EditTextPreference) findPreference("age");
        age.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener(){
            @Override
            public boolean onPreferenceChange(Preference preference, Object object){
                preference.setSummary(object.toString());
                return true;
            }
        });
    }

    /** Shared Preference */
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
    }
}