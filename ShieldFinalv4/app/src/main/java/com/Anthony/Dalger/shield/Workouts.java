package com.Anthony.Dalger.shield;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * Workouts Class
 * Displays workout categories, by body part, for
 * selection, leading to a workout lists.
 */

public class Workouts extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);

        /** Set Logo and Logo transparency */
        ImageView mainScreenLogo = (ImageView) findViewById(R.id.logoIMG);
        mainScreenLogo.setAlpha(50);
        mainScreenLogo.setImageResource(R.drawable.body_2);

        /** Set Logo dimensions based on screen orientation */
        int orientation = getResources().getConfiguration().orientation;
        if (Configuration.ORIENTATION_LANDSCAPE == orientation) {
            mainScreenLogo.getLayoutParams().height = 800;
            mainScreenLogo.getLayoutParams().width = 500;
        } else if (Configuration.ORIENTATION_PORTRAIT == orientation) {
            mainScreenLogo.getLayoutParams().height = 1600;
            mainScreenLogo.getLayoutParams().width = 1000;
        }

        /** Retrieve sources from layout */
        Button fullBodyBtn = (Button)findViewById(R.id.fullBodyBtn);
        Button shouldersBtn = (Button)findViewById(R.id.shouldersBtn);
        Button armsBtn = (Button)findViewById(R.id.armsBtn);
        Button chestBtn = (Button)findViewById(R.id.chestBtn);
        Button backBodyBtn = (Button)findViewById(R.id.backBodyBtn);
        Button absBtn = (Button)findViewById(R.id.absBtn);
        Button legsBtn = (Button)findViewById(R.id.legsBtn);

        /** View list of full body workouts */
        fullBodyBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentFullBody = new Intent(Workouts.this, WorkoutListActivity.class);
                intentFullBody.putExtra((getString(R.string.workout_ID)), (getString(R.string.full)));
                startActivity(intentFullBody);
            }
        });

        /** View list of shoulder workouts */
        shouldersBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentShoulders = new Intent(Workouts.this, WorkoutListActivity.class);
                intentShoulders.putExtra((getString(R.string.workout_ID)), (getString(R.string.shoulders)));
                startActivity(intentShoulders);
            }
        });

        /** View list of arm workouts */
        armsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentArms = new Intent(Workouts.this, WorkoutListActivity.class);
                intentArms.putExtra((getString(R.string.workout_ID)), (getString(R.string.arms)));
                startActivity(intentArms);
            }
        });

        /** View list of chest workouts */
        chestBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentChest = new Intent(Workouts.this, WorkoutListActivity.class);
                intentChest.putExtra((getString(R.string.workout_ID)), (getString(R.string.chest)));
                startActivity(intentChest);
            }
        });

        /** View list of back workouts */
        backBodyBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentBackBody = new Intent(Workouts.this, WorkoutListActivity.class);
                intentBackBody.putExtra((getString(R.string.workout_ID)), (getString(R.string.back)));
                startActivity(intentBackBody);
            }
        });

        /** View list of abdominal workouts */
        absBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentAbs = new Intent(Workouts.this, WorkoutListActivity.class);
                intentAbs.putExtra((getString(R.string.workout_ID)), (getString(R.string.abs)));
                startActivity(intentAbs);
            }
        });

        /** View list of leg workouts */
        legsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intentLegs = new Intent(Workouts.this, WorkoutListActivity.class);
                intentLegs.putExtra((getString(R.string.workout_ID)), (getString(R.string.legs)));
                startActivity(intentLegs);
            }
        });

    }

    /** Create Options Menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shield, menu);
        return true;
    }

    /** Add Option Menu Items */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /** Opens About class to view About Us */
            case R.id.menu_about_us:
                Intent aboutIntent = new Intent(Workouts.this, About.class);
                startActivity(aboutIntent);
                break;

            /** Directs the user to Shield Fitness Home page */
            case R.id.menu_website:
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.shield_website)));
                startActivity(i);
                break;

            /** Directs the user to Settings and Preferences */
            case R.id.menu_settings:
                Intent homeIntent = new Intent(Workouts.this, MyPreferenceActivity.class);
                startActivity(homeIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}