package com.Anthony.Dalger.shield;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * MyListActivity Class
 * Displays results of Products screen. This screen then
 * leads the user to the selected product in the next
 * screen.
 */

public class MyListActivity extends Shield
{
    /** declare web addresses final */
    private final String accessoriesURL = "http://shieldfitness.com.au/collections/accessories/products/";
    private final String beltsURL = "http://shieldfitness.com.au/collections/belts/products/";
    private final String glovesURL = "http://shieldfitness.com.au/collections/gloves-1/products/";
    private final String wrapsURL = "http://shieldfitness.com.au/collections/wraps-straps/products/";
    private final String packsURL = "http://shieldfitness.com.au/collections/gym-packs/products/";

    private String productID;
    ArrayList<String> resultsList;
    
    public void onCreate(Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_product_list);

        /** Receive value passed from Products screen */
        Bundle bundle = getIntent().getExtras();
        String importID = bundle.getString((getString(R.string.product_ID)));

        /** Convert received value to String */
        productID = importID.toString();

        /** Retrieve sources from layout */
        ListView productList = (ListView)findViewById(R.id.listViewResults);

        /** Create Array for results, get list */
        resultsList = new ArrayList<String>();
        getList();

        /** Create an Adapter; pass resultsList Array into the Adapter */
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, resultsList);
        productList.setAdapter(arrayAdapter);

        /* On List Item Click, Check to see which position the item is in the Array.
        Store the value of the List Item as selectedProduct, then match the product with string
        Depending on the string value, a link to the Shield Fitness Webpage is allocated
        */
        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                String selectedProduct = resultsList.get(position);
                Intent i = new Intent(MyListActivity.this, WebViewActivity.class);

                if (selectedProduct == (getString(R.string.accessories1))) {
                    i.putExtra((getString(R.string.web_ID)), accessoriesURL + "anklets-nylon");
                } else if (selectedProduct == (getString(R.string.accessories2))) {
                    i.putExtra((getString(R.string.web_ID)), accessoriesURL + "head-harness");
                } else if (selectedProduct == (getString(R.string.accessories3))) {
                    i.putExtra((getString(R.string.web_ID)), accessoriesURL + "neoprene-grabbers");
                } else if (selectedProduct == (getString(R.string.belts1))) {
                    i.putExtra((getString(R.string.web_ID)), beltsURL + "4-leather-shield-olympic-belt");
                } else if (selectedProduct == (getString(R.string.belts2))) {
                    i.putExtra((getString(R.string.web_ID)), beltsURL + "6-leather-shield-olympic-belt");
                } else if (selectedProduct == (getString(R.string.belts3))) {
                    i.putExtra((getString(R.string.web_ID)), beltsURL + "neoprene-dipping-belt");
                } else if (selectedProduct == (getString(R.string.belts4))) {
                    i.putExtra((getString(R.string.web_ID)), beltsURL + "shield-er-powerlifting-belt");
                } else if (selectedProduct == (getString(R.string.belts5))) {
                    i.putExtra((getString(R.string.web_ID)), beltsURL + "weight-training-shield-plus-belt");
                } else if (selectedProduct == (getString(R.string.belts6))) {
                    i.putExtra((getString(R.string.web_ID)), beltsURL + "weight-training-shield-plus-2-belt");
                } else if (selectedProduct == (getString(R.string.gloves1))) {
                    i.putExtra((getString(R.string.web_ID)), glovesURL + "ladies-leather-weightlifting-gloves");
                } else if (selectedProduct == (getString(R.string.gloves2))) {
                    i.putExtra((getString(R.string.web_ID)), glovesURL + "leather-weightlifting-gloves");
                } else if (selectedProduct == (getString(R.string.gloves3))) {
                    i.putExtra((getString(R.string.web_ID)), glovesURL + "leather-weightlifting-gloves-with-wrist-wrap");
                } else if (selectedProduct == (getString(R.string.wraps1))) {
                    i.putExtra((getString(R.string.web_ID)), wrapsURL + "lifting-straps-figure-8");
                } else if (selectedProduct == (getString(R.string.wraps2))) {
                    i.putExtra((getString(R.string.web_ID)), wrapsURL + "lifting-straps-with-padding");
                } else if (selectedProduct == (getString(R.string.wraps3))) {
                    i.putExtra((getString(R.string.web_ID)), wrapsURL + "knee-wrap");
                } else if (selectedProduct == (getString(R.string.wraps4))) {
                    i.putExtra((getString(R.string.web_ID)), wrapsURL + "lifting-hooks");
                } else if (selectedProduct == (getString(R.string.wraps5))) {
                    i.putExtra((getString(R.string.web_ID)), wrapsURL + "16-wrist-wrap");
                } else if (selectedProduct == (getString(R.string.wraps6))) {
                    i.putExtra((getString(R.string.web_ID)), wrapsURL + "lifting-straps-dowel");
                } else if (selectedProduct == (getString(R.string.packs1))) {
                    i.putExtra((getString(R.string.web_ID)), packsURL + "beginner-gym-pack");
                } else if (selectedProduct == (getString(R.string.packs2))) {
                    i.putExtra((getString(R.string.web_ID)), packsURL + "intermediate-gym-pack");
                } else if (selectedProduct == (getString(R.string.packs3))) {
                    i.putExtra((getString(R.string.web_ID)), packsURL + "advanced-gym-pack");
                } else if (selectedProduct == (getString(R.string.packs4))) {
                    i.putExtra((getString(R.string.web_ID)), packsURL + "gym-pack-4");
                } else if (selectedProduct == (getString(R.string.packs5))) {
                    i.putExtra((getString(R.string.web_ID)), packsURL + "powerlifting-pack");
                }
                startActivity(i);
            }
        });
    }

    void getList()
    {
        /** List Items added depending on what button was pressed on previous screen */
        if (productID.equals((getString(R.string.accessories)))) {
            resultsList.add((getString(R.string.accessories1)));
            resultsList.add((getString(R.string.accessories2)));
            resultsList.add((getString(R.string.accessories3)));
        }
        else if (productID.equals((getString(R.string.belts)))) {
            resultsList.add((getString(R.string.belts1)));
            resultsList.add((getString(R.string.belts2)));
            resultsList.add((getString(R.string.belts3)));
            resultsList.add((getString(R.string.belts4)));
            resultsList.add((getString(R.string.belts5)));
            resultsList.add((getString(R.string.belts6)));
        }
        else if (productID.equals((getString(R.string.gloves)))) {
            resultsList.add((getString(R.string.gloves1)));
            resultsList.add((getString(R.string.gloves2)));
            resultsList.add((getString(R.string.gloves3)));
        }
        else if (productID.equals((getString(R.string.wraps)))) {
            resultsList.add((getString(R.string.wraps1)));
            resultsList.add((getString(R.string.wraps2)));
            resultsList.add((getString(R.string.wraps3)));
            resultsList.add((getString(R.string.wraps4)));
            resultsList.add((getString(R.string.wraps5)));
            resultsList.add((getString(R.string.wraps6)));
        }
        else if (productID.equals((getString(R.string.packs)))) {
            resultsList.add((getString(R.string.packs1)));
            resultsList.add((getString(R.string.packs2)));
            resultsList.add((getString(R.string.packs3)));
            resultsList.add((getString(R.string.packs4)));
            resultsList.add((getString(R.string.packs5)));
        }
    }
}