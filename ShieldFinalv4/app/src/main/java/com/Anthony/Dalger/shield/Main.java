package com.Anthony.Dalger.shield;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/*
 * Created By: Anthony Dalger
 * Copyright (C) 2014 Shield Fitness
 *
 * Main Class
 * Displays a menu once the user has logged in. This screen
 * is the main menu of the app, and connects to all other
 * screens.
 */

public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /** Retrieve sources from layout */
        Button productsBtn = (Button)findViewById(R.id.productsBtn);
        Button workoutsBtn = (Button)findViewById(R.id.workoutsBtn);
        Button pedometerBtn = (Button)findViewById(R.id.pedometerBtn);
        ImageView mainScreenLogo = (ImageView) findViewById(R.id.logoIMG);

        /** Set Logo transparency */
        mainScreenLogo.setAlpha(50);

        /** Set Logo in background depending on screen orientation,
         * as well as width and height.
         */
        int orientation = getResources().getConfiguration().orientation;
        if (Configuration.ORIENTATION_LANDSCAPE == orientation) {
            mainScreenLogo.setImageResource(R.drawable.body_1_1);
            mainScreenLogo.getLayoutParams().height = 1200;
            mainScreenLogo.getLayoutParams().width = 800;
        } else if (Configuration.ORIENTATION_PORTRAIT == orientation) {
            mainScreenLogo.setImageResource(R.drawable.body_1);
            mainScreenLogo.getLayoutParams().height = 1800;
            mainScreenLogo.getLayoutParams().width = 1200;
        }

        /** Take the user to the products menu */
        productsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent productsIntent = new Intent(Main.this, Products.class);
                startActivity(productsIntent);
            }
        });

        /** Take the user to the workouts menu */
        workoutsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent workoutsIntent = new Intent(Main.this, Workouts.class);
                startActivity(workoutsIntent);
            }
        });

        /** Take the user to the pedometer */
        pedometerBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent pedometerIntent = new Intent(Main.this, Pedometer.class);
                startActivity(pedometerIntent);
            }
        });
    }

    /** Create Options Menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shield, menu);
        return true;
    }

    /** Add Option Menu Items */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /** Opens About class to view About Us */
            case R.id.menu_about_us:
                Intent aboutIntent = new Intent(Main.this, About.class);
                startActivity(aboutIntent);
                break;

            /** Directs the user to Shield Fitness Home page */
            case R.id.menu_website:
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.shield_website)));
                startActivity(i);
                break;

            /** Directs the user to Settings and Preferences */
            case R.id.menu_settings:
                Intent homeIntent = new Intent(Main.this, MyPreferenceActivity.class);
                startActivity(homeIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}