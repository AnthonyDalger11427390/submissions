package com.id11427390.exercise4;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.os.Handler;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;


public class Exercise4 extends Activity {

    String[] names;

    int progressIncrement = 1;
    ProgressDialog progressDialog;
    private static final int START = 1;
    private static final int COUNTING = 2-4;
    private static final int STOP = 5;

    Handler handler = new Handler () {
        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case START:
                    progressDialog = new ProgressDialog(Exercise4.this);
                    progressDialog.setMessage("Downloading...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressDialog.setMax(5);
                    progressDialog.setProgress(0);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    break;
                case COUNTING:
                    progressDialog.incrementProgressBy(1);
                    break;
                case STOP:
                    progressDialog.dismiss();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise4);

        names = getResources().getStringArray(R.array.names_array);
        Spinner newSpinner = (Spinner) findViewById(R.id.spinnerMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, names);

        newSpinner.setAdapter(adapter);
        newSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int index = parent.getSelectedItemPosition();

                Context context = getApplicationContext();
                CharSequence text = "Selected: " + names[index];
                int duration = Toast.LENGTH_SHORT;

                Toast.makeText(context, text, duration).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void downloadOneClick(View v) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(5);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressIncrement = 0;

        new Thread(new Runnable() {
            public void run() {
                while (progressIncrement <= 5) {
                    progressIncrement = progressDelay();
                }
            }

            //slows down the progress of the bar so it doesn't jump to 100% automatically
            private int progressDelay() {
                try {
                    progressDialog.setProgress(progressIncrement);
                    //sleep is delay time
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (progressIncrement == 5) {
                    progressDialog.dismiss();
                }
                return ++progressIncrement;
            }
        }).start();
    }

    public void downloadTwoClick(View v) {
        new Thread(new Runnable() {
            public void run() {
                handler.sendEmptyMessage(progressIncrement);
                while (progressIncrement < 5) {
                    ++progressIncrement;
                }
            }
        }).start();
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise4, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
