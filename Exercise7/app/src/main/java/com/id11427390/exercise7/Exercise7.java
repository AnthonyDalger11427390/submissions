package com.id11427390.exercise7;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class Exercise7 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise7);
    }

    // Start Button Pressed
    public void startOnClick(View view) {
        startService(new Intent(this, MyService.class));
    }

    // Stop Button Pressed
    public void stopOnClick(View view) {
        stopService(new Intent(this, MyService.class));
    }

    // Download Button Pressed - simulates MyService running
    public void downloadOnClick(View view) {
        //Print in LogCat for test purposes
        Log.i(null, "Download being requested");

        //Checks to see if service is running
        if(isMyServiceRunning(MyService.class)){
            Intent downloadIntent = new Intent("com.id11427390.Exercise7.DOWNLOAD");
            sendBroadcast(downloadIntent);
        }
        else
        {
            //No Service message displayed
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.noService),Toast.LENGTH_LONG).show();
        }
    }

        //Boolean check to see whether the service is running or not
        private boolean isMyServiceRunning(Class<?> serviceClass) {
            ActivityManager manager = (ActivityManager)
                    getSystemService(Context.ACTIVITY_SERVICE);

            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
            return false;
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise7, menu);
        return true;
    }
}
