package com.id11427390.exercise7;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service {

    MediaPlayer mMediaPlayer;
    private BroadcastReceiver mReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(null, "received");
        }
    };

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Media Player not yet initialised");
    }

    @Override
    public void onCreate() {
        mMediaPlayer = MediaPlayer.create(this, R.raw.braincandy);
        mMediaPlayer.setLooping(false);
        Toast.makeText(this, "Media Player initialised", Toast.LENGTH_SHORT).show();
        Log.d(null, "Media Player is now initialising");

				// Ryan: Don't hard-code this string. Define in Constants.java
        IntentFilter filter = new IntentFilter("com.id11427390.Exercise7.DOWNLOAD");
        registerReceiver(mReceiver, filter);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Toast.makeText(this, "Music Now Playing", Toast.LENGTH_SHORT).show();
        Log.d(null, "Media Player is now playing the selected music");
        mMediaPlayer.start();
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Music Stopped", Toast.LENGTH_SHORT).show();
        Log.d(null, "Media Player has stopped playing music");
        mMediaPlayer.stop();
    }
}
