package com.id11427390.exercise5;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;


public class Exercise5 extends Activity {

    EditText number;
    EditText name;
    private Context exercise5 = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise5);
        number = (EditText) findViewById(R.id.subjectNumber);
        name = (EditText) findViewById(R.id.subjectName);
        registerForContextMenu(name);
        
        Button listBtn = (Button)findViewById(R.id.showListButton);
        listBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent subjectIntent = new Intent(exercise5, Subject.class);
                startActivity(subjectIntent);
            }
        });

        //ListView listView = (ListView) findViewById(R.id.listView);
        //String[] newList = new String[] {"iPhone", "iPad", "iPad mini", "iPod"};

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                //android.R.layout.simple_list_item_1, newList);

        //listView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
        //listView.setOnItemClickListener(this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater contextInflater = getMenuInflater();
        contextInflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.menu_capitalize:
                String str = name.getText().toString();
                name.setText(str.toUpperCase());
                return true;
            case R.id.menu_copy:
                return true;
            case R.id.menu_clear:
                name.setText("");
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.exercise5, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_email:
                break;
            case R.id.menu_help:
                String takeSubjectNumber = number.getText().toString();
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://handbook.uts.edu.au/subjects/" + takeSubjectNumber));
                startActivity(i);
                break;
            case R.id.menu_exit:
                finish();

                //System.exit(0);

                //finish();
                //android.os.Process.killProcess(android.os.Process.myPid());
                //super.onDestroy();
                break;
        }
        return true;
    }


    public abstract class SubjectList extends ListActivity implements View.OnClickListener {

    }

    public class SubjectAdapter {

    }
}