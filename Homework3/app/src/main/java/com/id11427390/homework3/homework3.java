package com.id11427390.homework3;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;
import android.view.View.OnClickListener;


public class homework3 extends Activity {

    final String madText = "MAD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework3);
        RadioButton coreCheck = (RadioButton) findViewById(R.id.coreRadio);
        RadioButton electiveCheck = (RadioButton) findViewById(R.id.electiveRadio);
        coreCheck.setChecked(true);
        //set the core radio button to be checked by default
        Button mQuitBtn = (Button)findViewById(R.id.quitBtn);
        Button rotate = (Button) findViewById(R.id.rotateBtn);
        mQuitBtn.setOnClickListener(btnListener);
        final EditText name = (EditText) findViewById(R.id.subjectName);
        EditText number = (EditText) findViewById(R.id.subjectNumber);
        ImageView logo = (ImageView) findViewById(R.id.logoImage);
        logo.setImageResource(R.drawable.ic_launcher);
        //sets the image from drawable

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                //check to see if the name field has focus or not
                if (hasFocus) {
                    Context context = getApplicationContext();
                    CharSequence text = "Subject name HAS focus";
                    int duration = Toast.LENGTH_SHORT;
                    //toast to show that subject name now has focus
                    Log.d(madText, "MAD");
                    Toast.makeText(context, text, duration).show();
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Subject name LOST focus";
                    int duration = Toast.LENGTH_SHORT;
                    //toast to show that subject name no longer has focus
                    Log.d(madText, "MAD");
                    Toast.makeText(context, text, duration).show();
                }

            }
        });

        rotate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onRotateClicked(v);
            }

            public void onRotateClicked(View v) {
                //check to see what the current orientation is
                int orientation = getResources().getConfiguration().orientation;
                //simple if statement to switch between orientations
                if (Configuration.ORIENTATION_LANDSCAPE == orientation) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else if (Configuration.ORIENTATION_PORTRAIT == orientation) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
            }
        });
    }

    private OnClickListener btnListener = new OnClickListener() {
        public void onClick(View v) {

        }
    };

    public View.OnFocusChangeListener focusChangeListener() {
        return focusChangeListener();
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homework3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
