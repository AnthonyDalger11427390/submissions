package com.id11427390.homework4;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.os.Handler;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;


public class Homework4 extends Activity {

    String[] names;

    int progressIncrement = 1;
    ProgressDialog progressDialog;
    private static final int START = 1;
    private static final int COUNTING = 2-4;
    private static final int STOP = 5;

    int totalTime;
    int incrementValue;
    int sleepInterval;

    Handler handler = new Handler () {
        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case START:
                    progressDialog = new ProgressDialog(Homework4.this);
                    progressDialog.setMessage("Downloading...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressDialog.setMax(5);
                    progressDialog.setProgress(0);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    break;
                case COUNTING:
                    progressDialog.incrementProgressBy(1);
                    break;
                case STOP:
                    progressDialog.dismiss();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework4);

        names = getResources().getStringArray(R.array.names_array);
        Spinner newSpinner = (Spinner) findViewById(R.id.spinnerMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, names);

        newSpinner.setAdapter(adapter);
				// Ryan: You were asked to make the activity implement this interface rather than use an anonymous inner class.
        newSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int index = parent.getSelectedItemPosition();

                Context context = getApplicationContext();
                CharSequence text = "Selected: " + names[index];
                int duration = Toast.LENGTH_SHORT;

                Toast.makeText(context, text, duration).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void downloadOneClick(View v) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(5);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressIncrement = 0;

        new Thread(new Runnable() {
            public void run() {
                while (progressIncrement <= 5) {
									// Ryan: This is a confusing assignment given the side effect of this method. See below.
                    progressIncrement = progressDelay();
                }
            }

            //slows down the progress of the bar so it doesn't jump to 100% automatically
            private int progressDelay() {
                try {
                    progressDialog.setProgress(progressIncrement);
                    //sleep is delay time
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (progressIncrement == 5) {
                    progressDialog.dismiss();
                }
								// Ryan: This is a confusing side effect since the variable is assigned elsewhere - see above.
                return ++progressIncrement;
            }
        }).start();
    }

    public void downloadTwoClick(View v) {
        new Thread(new Runnable() {
            public void run() {
                handler.sendEmptyMessage(progressIncrement);
                while (progressIncrement < 5) {
                    ++progressIncrement;
                }
            }
        }).start();
    };

    public void downloadThreeClick(View v) {
        //values set for incrementing progressbar
        totalTime = 200;
        incrementValue = 2;
        sleepInterval = 1000;

        //progressbar is created, and variables are set
        progressDialog = new ProgressDialog(Homework4.this);
        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(totalTime);
        progressDialog.setProgress(incrementValue);
        progressDialog.setCancelable(false);
        progressDialog.show();

        //set increment to zero
        progressIncrement = 0;

        //create MyAsyncTask/set parameters
        new MyAsyncTask().execute(totalTime, incrementValue, sleepInterval);
    };

    private class MyAsyncTask extends AsyncTask <Integer, Integer, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            //increment happens in background
            while (progressIncrement < totalTime) {
                progressDialog.incrementProgressBy(incrementValue);
                try {
                    Thread.sleep(sleepInterval);
                    publishProgress(progressIncrement);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ++progressIncrement;
								// Ryan: Should use "publishProgress" here so that onProgressUpdate is called on the UI thread.
                onProgressUpdate();
            }
            return null;
        }

				// Ryan: you called this directly so it won't run in the UI thread, and you also failed to pass in any parameters
				// (which get ignored here anyway).
				// What you want is to use the parameter to indicate perhaps what progress you are up to,
				// and you want to invoke it with publishProgress(...)
        protected void onProgressUpdate(Integer... progress){
            super.onProgressUpdate(progress);
            //close progressbar when finished (not working)
            if (progressIncrement == totalTime) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homework4, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
